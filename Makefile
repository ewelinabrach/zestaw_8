CC=gcc
CFLAGS=-Wall
LDLIBS=-lm

all: main main2

main: main.o srednia.o odst.o mediana.o 
	$(CC) $(CFLAGS) -o main main.o srednia.o odst.o mediana.o  $(LDLIBS)

main.o: main.c naglowkowy.h
	$(CC) $(CFLAGS) -c main.c

main2: main2.o srednia.o odst.o mediana.o 
	$(CC) $(CFLAGS) -o main2 main2.o srednia.o odst.o mediana.o  $(LDLIBS)

main2.o: main2.c naglowkowy.h
	$(CC) $(CFLAGS) -c main2.c

srednia.o: srednia.c
	$(CC) $(CFLAGS) -c srednia.c

odst.o: odst.c
	$(CC) $(CFLAGS) -c odst.c

mediana.o: mediana.c 
	$(CC) $(CFLAGS) -c mediana.c 
