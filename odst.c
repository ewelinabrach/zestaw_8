#include "naglowkowy.h"
#include <math.h>
float odst(float *tab, float sred, int n)
{
    int i; double tmp=0.0;
    for(i=0; i<n; i++)
        tmp+=pow(tab[i]-sred, 2);

    double wariacja=tmp/50;                 //wariacja
    double odch=sqrt(wariacja);              //odchylenie standardowe
    float odchylenie= (double)odch;
    return odchylenie;
}

