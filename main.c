#include "naglowkowy.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define EXIT_FAILURE 1

struct pomiar
{
    char srednia;
    char mediana;
    char odchylenie;

};
void wczytaj (float *tabX, float *tabY, float *tabRHO, int n)
{
    int i=0;
    FILE *file;
    file = fopen("P0001_attr.rec", "r+");
    int tablica[50];
    if(file == NULL)
    {
        printf("Blad, brak daych. Zakonczenie programu");
        exit(EXIT_FAILURE);
    }
    fseek(file, 13, SEEK_SET);
    for(i; i<n; i++) //wczytywanie odpowiednich danych do odpowednich zmiennych
    {
        fscanf(file,"%d.\t%f \t%f\t%f\n", &tablica[i], &tabX[i],&tabY[i], &tabRHO[i]);
    }
    fclose(file);
    return;
}
void wyswietl(float *tabX, float *tabY, float *tabRHO, int n)
{
    printf("LP\tX\t\tY\t\tRHO\n");
    int i=0;
    for(i; i<n; i++)
    {
        printf("%d\t%.5f\t\t%.5f\t%.3f\n",i+1, tabX[i],tabY[i], tabRHO[i]);
    }
    return;

}

int main()
{
    FILE*file;

    struct pomiar sredniaX;
    struct pomiar sredniaY;
    struct pomiar sredniaRHO;
    struct pomiar medianaX;
    struct pomiar medianaY;
    struct pomiar medianaRHO;
    struct pomiar odchylenieX;
    struct pomiar odchylenieY;
    struct pomiar odchylenieRHO;

    int n=50;
    float a=0.0,b=0.0,c=0.0,d=0.0,e=0.0,f=0.0,g=0.0,h=0.0,k=0.0;
    float *tabX=calloc(n, sizeof(float));
    float *tabY=calloc(n, sizeof(float));
    float *tabRHO=calloc(n, sizeof(float));
    wczytaj(tabX,tabY,tabRHO,n);
    wyswietl(tabX,tabY,tabRHO,n);
    a=srednia(tabX,n);
    b=srednia(tabY,n);
    c=srednia(tabRHO,n);
    d=odst(tabX,a,n);
    e=odst(tabY, b,n);
    f=odst(tabRHO,c,n);

    g=mediana(tabX,n);
    h=mediana(tabY,n);
    k=mediana(tabRHO,n);

    sredniaX.srednia=a;
    sredniaY.srednia=b;
    sredniaRHO.srednia=c;

    medianaX.mediana=g;
    medianaY.mediana=h;
    medianaRHO.mediana=k;

    odchylenieX.odchylenie=d;
    odchylenieY.odchylenie=e;
    odchylenieRHO.odchylenie=f;

    printf("\nsrednia: %f %f %f, \nodchylenie: %f %f %f  ",a,b,c,d,e,f);
    printf("\nmediana: %f %f %f",g,h,k);
    file=fopen("P0001_attr.rec","ab");

    fprintf(file,"\nsrednia: %f %f %f \nodchylenie: %f %f %f\nmediana: %f %f %f",a,b,c,d,e,f,g,h,k);
    fclose(file);


    int sprawdzenie=0;
    if((fgetc(file))!= EOF)
    {
        sprawdzenie++;
    }
    if(sprawdzenie==0)
    {
        file=("P0001_attr.rec", "a");
        fprintf(file, "\n KOLUMNY    X   Y   RHO");
        fprintf(file, "\n srednia   %f   %f   %f",a,b,c);
        fprintf(file, "\n odchylenie    %f   %f   %f",d,e,f);
        fprintf(file, "\n mediana    %f   %f   %f",g,h,k);
        fclose(file);
    }

    return 0;
}

